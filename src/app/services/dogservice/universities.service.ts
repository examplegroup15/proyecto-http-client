import {Injectable} from '@angular/core';
import { HttpClient} from  '@angular/common/http'
import { University} from 'src/app/models/universities/universities.model';
import { observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})

export class universityservice{
    constructor(
        private http: HttpClient){}

        GetUni(url: string){
            return this.http.get<University[]>("http://universities.hipolabs.com/search?country=United+States");
        }
   

}