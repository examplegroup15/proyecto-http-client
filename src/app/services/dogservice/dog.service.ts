import {Injectable} from '@angular/core';
import { HttpClient} from  '@angular/common/http'
import { dog } from 'src/app/models/dog/dog.model';
import { observable } from 'rxjs';
 
@Injectable({
    providedIn:'root'
})

export class Dogservice{
    constructor(
        private http: HttpClient){}

        GetDog(url: string){
            return this.http.get<dog>("https://dog.ceo/api/breeds/image/random");
        }
   

}