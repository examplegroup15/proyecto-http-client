export class University{
    web_pages: string[]
    state_province: boolean
    alpha_two_code: string
    name: string
    country: string
    domains: string[]

    constructor(){
        this.web_pages= [];
        this.state_province= false;
        this.alpha_two_code="";
        this.name= "";
        this.country="";
        this.domains=[];
    }
}


/*{
    "web_pages": [
        "http://www.marywood.edu"
    ],
    "state-province": null,
    "alpha_two_code": "US",
    "name": "Marywood University",
    "country": "United States",
    "domains": [
        "marywood.edu"
    ]
},

*/