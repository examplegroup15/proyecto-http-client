import { Component, OnInit } from '@angular/core';
import { JsonService } from './json.service';
import { dog } from './models/dog/dog.model';
import { University } from './models/universities/universities.model';
import { Dogservice } from './services/dogservice/dog.service';
import { universityservice } from './services/dogservice/universities.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent {
  unis: University[] = [];
  title = 'proyecto-http-client';
  constructor(public json: universityservice){
    this.json.GetUni("https://official-joke-api.appspot.com/random_joke").subscribe((res: University[]) => {
      this.unis=res;
      
    })
    
  }

  
    //la respuesta de la api de los perros
    /*constructor(public json: Dogservice){
      this.json.GetDog("https://official-joke-api.appspot.com/random_joke").subscribe((res: dog) => {
        console.log("MENSAJE DE PERRO",res.message);
      })
    }*/
  
    //la respuesta de la api de los chistes 
  /*constructor(public json: JsonService){
    this.json.GetJson("https://official-joke-api.appspot.com/random_joke").subscribe((res: any) => {
      console.log(res);
    })
  }*/


}
